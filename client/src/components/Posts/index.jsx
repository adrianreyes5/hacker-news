import React from "react";

import Post from "../Post";

// styles
import "./styles.scss";

const Home = ({ posts = [], handleDeletePost, deletedPosts = [] }) => {
  return posts.length > 0 ? (
    posts.map(
      (post, key) =>
        // Check if the post is deleted
        !deletedPosts.includes(post.objectID) && (
          <React.Fragment key={key}>
            <Post post={post} handleDeletePost={handleDeletePost} />
          </React.Fragment>
        )
    )
  ) : (
    <div className="empty-text">
      <h2>There aren't post yet</h2>
    </div>
  );
};

export default Home;
