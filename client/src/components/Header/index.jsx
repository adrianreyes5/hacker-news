import React from "react";

// styles
import "./styles.scss";

const Header = () => {
  return (
    <header className="header">
      <div className="header_wrapper">
        <h1 className="header_title">HN feed</h1>
        <h4 className="header_sub-title">{"We <3 hacker news!"}</h4>
      </div>
    </header>
  );
};

export default Header;
