import React, { useMemo } from "react";
import moment from "moment";

import DeleteIcon from "../../assets/svg/delete.svg";

// styles
import "./styles.scss";

const Post = ({
  post = {
    _id: "",
    title: "",
    story_title: "",
    story_url: "",
    url: "",
    created_at: "",
    objectID: "",
    author: "",
  },
  handleDeletePost,
}) => {
  // Parse dates according to the task
  const createdAt = useMemo(() => {
    return moment(post.created_at).calendar(null, {
      sameDay: "LT",
      lastDay: "[Yesterday]",
      lastWeek: "MMMM DD",
      sameElse: "MMMM DD",
    });
  }, [post.created_at]);

  return (
    <>
      {(post.title || post.story_title) && (
        <>
          <div className="post">
            <div className="post_text">
              <a
                href={post.story_url || post.url}
                target="_blank"
                className="post_link"
                rel="noreferrer"
              >
                {post.title || post.story_title}{" "}
                <span className="post_text--author"> - {post.author} - </span>
              </a>
            </div>
            <div className="post_actions">
              <div className="post_text">{createdAt}</div>
              <div
                className="post_actions--img-wrapper"
                onClick={() => handleDeletePost(post._id, post.objectID)}
              >
                <img src={DeleteIcon} alt="delete" width={25} />
              </div>
            </div>
          </div>
          <div className="post_border" />
        </>
      )}
    </>
  );
};

export default Post;
