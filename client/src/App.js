import { useCallback, useEffect, useState } from "react";

import { deletePost, getPosts, getDeletePosts } from "./api/posts";

import Header from "./components/Header";
import Home from "./components/Posts";

function App() {
  const [posts, setPosts] = useState([]);
  const [deletedPosts, setDeletedPosts] = useState([]);

  // get all post on first render
  useEffect(() => {
    const getData = async () => {
      const data = await getPosts();

      setPosts(data);

      const deletedPosts = await getDeletePosts();

      const ids = deletedPosts?.map(deletedPost => deletedPost.objectID);
      setDeletedPosts(ids);
    };

    getData();
  }, []);

  // Event to delete a post
  const handleDeletePost = useCallback(
    async id => {
      const { status } = await deletePost(id);

      if (status === 204) {
        const data = await getPosts();

        setPosts(data);

        const deletedPosts = await getDeletePosts();

        const ids = deletedPosts?.map(deletedPost => deletedPost.objectID);
        setDeletedPosts(ids);
      } else {
        alert("There is an error.");
      }
    },
    [setDeletedPosts]
  );

  return (
    <div>
      <Header />
      <Home
        posts={posts}
        handleDeletePost={handleDeletePost}
        deletedPosts={deletedPosts}
      />
    </div>
  );
}

export default App;
