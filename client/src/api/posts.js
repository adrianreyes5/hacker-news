import axios from "../config/axios";

// fetch all posts
export const getPosts = async () => {
  try {
    const { data } = await axios.get("posts");

    return data;
  } catch (error) {
    throw error;
  }
};

// delete a post
export const deletePost = async id => {
  try {
    const data = await axios.delete(`posts/${id}`);

    return data;
  } catch (error) {
    throw error;
  }
};

// get deleted posts
export const getDeletePosts = async id => {
  try {
    const { data } = await axios.get(`posts/deleted`);

    return data;
  } catch (error) {
    throw error;
  }
};
