to build client and server images, from root folder run:

docker compose up -d --build

It will setup the client (ReactJS) and server (NestJS) and the database (mongodb)

In the first render on server, the Post service will call the external API and it will get the first 20 posts. Then for each hour there is a cron
that is executed.