export class CreatePostDto {
  readonly story_title: string;
  readonly title: string;
  readonly story_url: string;
  readonly created_at: string;
  readonly author: string;
}
