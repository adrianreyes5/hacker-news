import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema } from 'mongoose';
import { Posts } from '../schema/post.schema';
import { PostDeleted } from '../schema/postDeleted.schema';
import { CreatePostDto } from '../dto/post.dto';

@Injectable()
export class PostRepository {
  constructor(
    @InjectModel('Posts') private readonly postModel: Model<Posts>,
    @InjectModel('DeletedPost')
    private readonly postDeleted: Model<PostDeleted>,
  ) {}

  /**
   * Create post
   * @param posts
   * @returns posts
   */
  async createPost(posts: CreatePostDto[]): Promise<any> {
    posts.map(async (post) => {
      const newPost = new this.postModel(post);

      await newPost.save();

      try {
        return await newPost.save();
      } catch (e) {
        throw new InternalServerErrorException(e.message);
      }
    });

    return posts;
  }

  /**
   * get all posts in the db
   * @returns posts
   */
  async findAllPosts(): Promise<Posts[]> {
    const post = this.postModel.find({});

    return post;
  }

  /**
   * get all deleted posts in the db
   * @returns posts
   */
  async findDeletedPosts(): Promise<PostDeleted[]> {
    const post = this.postDeleted.find({});

    return post;
  }

  /**
   * delete a posts
   */
  async deletePost(_id: string): Promise<Posts> {
    const post = await this.postModel.findByIdAndDelete(_id);

    if (!post) {
      throw new NotFoundException(`Post with '${_id}' not found.`);
    }

    const postDeleted = new this.postDeleted({ objectID: post.objectID });

    await postDeleted.save();

    return post;
  }
}
