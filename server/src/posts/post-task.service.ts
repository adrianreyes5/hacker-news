import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PostService } from './post.service';

@Injectable()
export class PostTaskService {
  constructor(private readonly postService: PostService) {}

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    console.log('hora', new Date());
    this.postService.createPosts();
  }
}
