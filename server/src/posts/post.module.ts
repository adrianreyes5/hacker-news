import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { PostController } from './post.controller';
import { PostService } from './post.service';
import { PostTaskService } from './post-task.service';
import { PostRepository } from './repositories/post.repository';
import { PostSchema } from './schema/post.schema';
import { PostDeletedSchema } from './schema/postDeleted.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Posts', schema: PostSchema }]),
    MongooseModule.forFeature([{ name: 'DeletedPost', schema: PostDeletedSchema }]),
  ],
  controllers: [PostController],
  providers: [PostService, PostTaskService, PostRepository],
})
export class PostModule {}
