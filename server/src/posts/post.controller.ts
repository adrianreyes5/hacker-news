import { Controller, Delete, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';

// service
import { PostService } from './post.service'; 

@Controller('posts')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Get()
  async findAllPost(): Promise<any> {
    const posts = await this.postService.findAllPosts();

    const sortedPosts = posts.sort(
      (a, b) =>
        new Date(b.created_at).getTime() - new Date(a.created_at).getTime(),
    );

    return sortedPosts;
  }

  @Get('deleted')
  async findDeletedPost(): Promise<any> {
    const posts = await this.postService.finDeletedPosts();

    return posts;
  }

  @Delete(':id')
  async deletePost(
    @Param('id') _id: string,
    @Res() response: Response,
  ): Promise<Response> {
    await this.postService.deletePost(_id);

    return response.status(204).json();
  }
}
