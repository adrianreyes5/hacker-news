import { Injectable, OnModuleInit } from '@nestjs/common';
import axios from 'axios';

import { PostRepository } from './repositories/post.repository';

@Injectable()
export class PostService implements OnModuleInit {
  constructor(private readonly postRepository: PostRepository) {}

  // create posts on first render
  onModuleInit() {
    console.log('init', new Date());
    this.createPosts();
  }

  /**
   * create post from external api
   * @returns posts
   */
  async createPosts() {
    const { data } = await axios.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );

    return this.postRepository.createPost(data.hits);
  }

  /**
   * find posts in db
   * @returns posts
   */
  async findAllPosts() {
    return this.postRepository.findAllPosts();
  }

  /**
   * find deleted post ids in db
   * @returns deleted posts
   */
  async finDeletedPosts() {
    return this.postRepository.findDeletedPosts();
  }

  /**
   * delete a post
   * @param _id
   * @returns empty
   */
  async deletePost(_id: string) {
    return this.postRepository.deletePost(_id);
  }
}
