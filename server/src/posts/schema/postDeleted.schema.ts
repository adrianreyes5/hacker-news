import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

/**
 * Mongoose Document that represents the 'deleted posts' collection in the DB.
 */
@Schema()
export class PostDeleted extends Document {
  @Prop()
  objectID: string;
}

/**
 * Create Mongoose posts from Draw document.
 */
export const PostDeletedSchema = SchemaFactory.createForClass(PostDeleted);
