import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

/**
 * Mongoose Document that represents the 'posts' collection in the DB.
 */
@Schema()
export class Posts extends Document {
  @Prop()
  story_title: string;

  @Prop()
  title: string;

  @Prop()
  story_url: string;

  @Prop()
  url: string;

  @Prop()
  created_at: string;

  @Prop()
  author: string;

  @Prop()
  objectID: string;

  @Prop()
  story_id: string;
}

/**
 * Create Mongoose posts from Draw document.
 */
export const PostSchema = SchemaFactory.createForClass(Posts);
